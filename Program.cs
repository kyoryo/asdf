﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();
            MyLinkedList<int> list = new MyLinkedList<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(5);
            list.Add(6);
            //list[1] = 333;
            //list.remove(1);
            list.Remove(2);
            foreach (var item in list)
            {
                Console.WriteLine(item);
            }
            Console.ReadKey();
        }
    }

    class MyLinkedList<T>
    {
        public Node firstNode;
        public Node lastNode;
        public int length;

        public MyLinkedList()
        {
            firstNode = null;
            lastNode = firstNode;
        }

        public void Add(T data)
        {
            if (firstNode == null)
            {
                firstNode = new Node();
                firstNode.data = data;
                lastNode = firstNode;
                lastNode.nextNode = null;
            }
            else
            {
                Node newNode = new Node();
                newNode.data = data;
                lastNode.nextNode = newNode;
                lastNode = lastNode.nextNode;
                lastNode.nextNode = null;
            }
            length++;
        }

        public bool Remove(int Position)
        {
            Node currentNode = new Node();

            if (Position == 0)
            {
                firstNode = firstNode.nextNode;
                currentNode = firstNode;
                while (currentNode != null)
                {
                    lastNode = currentNode;
                    currentNode = currentNode.nextNode;
                }
                return true;
            }

            if (Position > 0 && Position <= length)
            {
                Node tempNode = firstNode;

                Node lastNode = null;
                int count = 0;

                while (tempNode != null)
                {
                    if (count == Position)
                    {
                        lastNode.nextNode = tempNode.nextNode;
                        return true;
                    }
                    count++;

                    lastNode = tempNode;
                    tempNode = tempNode.nextNode;
                }
            }

            return false;
        }

        public IEnumerator<T> GetEnumerator()
        {
            Node tempNode = firstNode;

            while (tempNode != null)
            {
                yield return tempNode.data;
                tempNode = tempNode.nextNode;
            }
        }

        public T this[int num]
        {
            get
            {
                Node tempNode = firstNode;
                for (int i = 0; i < num; i++)
                {
                    tempNode = tempNode.nextNode;
                }
                return tempNode.data;
            }
            set
            {
                Node tempNode = firstNode;
                for (int i = 0; i < num; i++)
                {
                    tempNode = tempNode.nextNode;
                }
                tempNode.data = value;
            }
        }

        public class Node
        {
            public Node nextNode;
            public T data;
        }
    }
}
